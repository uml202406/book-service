<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<!-- DTO를 참조하기 위해서 import 처리가 필요하다. -->
<%@ page import="com.ssafy.ws.step3.dto.Book"%>
<%-- jstl의 core 라이브러리를 사용하기 위해 taglib를 이용한다. --%>
<%@ taglib prefix="c" uri="jakarta.tags.core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>도서 등록 결과</title>
<style>
table {
	border-collapse: collapse;
	width: 100%;
}

th, td {
	border: 1px solid black;
}

th:nth-child(1) {
	width: 100px;
}
</style>
</head>
<body>
	<%@ include file="/include/header.jsp"%>
	<h1>도서 등록 결과</h1>
	<h2>등록 도서 정보</h2>
	<%-- c:if 태그를 이용해 request 영역에 book이 있다면 내용을 출력한다. --%>
	<c:if test="${!empty book }">
	<form method="post" action="${root}/books">
		<input type="hidden" name="action" value="update"/>
		<input type="hidden" name="isbn"  value="${book.isbn }" />
		<table>
			<thead>
				<tr>
					<th>항목</th>
					<th>내용</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>도서번호</td>
					<td>${book.isbn }</td>
				</tr>
				<tr>
					<td>도서명</td>
					<td><input type="text" name="title" value="${book.title }"/></td>
				</tr>
				<tr>
					<td>저자</td>
					<td><input type="text" name="author" value="${book.author }"/></td>
				</tr>
				<tr>
					<td>가격</td>
					<td><input type="number" name="price" value="${book.price }"/></td>
				</tr>
				<tr>
					<td>설명</td>
					<td>
						<textarea name="desc">
							${book.desc }
						</textarea>	
					</td>
				</tr>
			</tbody>
		</table>
		<input type="submit" value="수정하기"/>
		<!-- 다시 도서를 등록할 수 있는 링크를 제공한다. -->
		<a href="${root}/books?action=delete&isbn=${book.isbn}">삭제하기</a> &nbsp;	&nbsp;	
		<a href="${root}/books?action=registForm">추가등록</a>		
		</form>
	</c:if>
	<%-- c:if 태그를 이용해 request 영역에 book이 없다면 정보가 없음을 출력한다. --%>
	<c:if test="${empty book }">
		<p>등록된 도서가 없습니다.</p>
	</c:if>

</body>
</html>