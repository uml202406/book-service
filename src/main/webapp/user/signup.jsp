<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>SSAFY 사용자 등록</title>
<style>
	label{
		display: inline-block;
		width: 80px;
	}
	textarea {
	width: 100%;
}
</style>
</head>
<body>
	<%@ include file="/include/header.jsp"%>
	<h1>SSAFY 사용자 등록</h1>
	<form method="post" action="${root}/users">
		<fieldset>
		<legend>사용자 등록</legend>
		<!-- front-controller pattern에서 요청을 구분하기 위한 parameter -->
		<input type="hidden" name="action" value="signup">
		<label for="id">아이디</label>
		<input type="text" id="id" name="id"><br>
		<label for="name">이름</label>
		<input type="text" id="name" name="name"><br>
		<label for="author">암호</label>
		<input type="password" id="pass" name="pass"/><br/>
		<input type="submit" value="회원가입">
		<input type="reset" value="취소">
		</fieldset>
	</form>
</body>
</html>