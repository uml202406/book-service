package com.ssafy.ws.step3.dao;

import java.sql.SQLException;

import com.ssafy.ws.step3.dto.User;

public interface UserDao {
    /**
     * id에 해당하는 사용자 정보를 반환한다.
     *
     * @param id 조회할 사용자의 id
     * @return 조회된 사용자 정보 객체 User
     * @throws SQLException
     */
    User select(String id) throws SQLException;
    
    /**
     * 회원가입
     * @param user 가입할 회원정보
     * @return
     * @throws SQLException
     */
    int signup(User user) throws SQLException;
}
