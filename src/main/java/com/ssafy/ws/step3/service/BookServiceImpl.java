package com.ssafy.ws.step3.service;

import java.sql.SQLException;
import java.util.List;

import com.ssafy.ws.step3.dao.BookDao;
import com.ssafy.ws.step3.dao.BookDaoImpl;
import com.ssafy.ws.step3.dto.Book;

public class BookServiceImpl implements BookService {
	private static BookService instance = new BookServiceImpl();
	private BookDao dao;
	
	private BookServiceImpl() {
		dao = BookDaoImpl.getInstance();
	}
	
	public static BookService getInstance() {
		return instance;
	}

	@Override
	public int insert(Book book) throws SQLException {
		return dao.insert(book);
	}

	@Override
	public List<Book> select() throws SQLException {
		// TODO Auto-generated method stub
		return dao.select();
	}

	@Override
	public Book findById(String isbn) throws SQLException {
		// TODO Auto-generated method stub
		return dao.findById(isbn);
	}

	@Override
	public int update(Book book) throws SQLException {
		// TODO Auto-generated method stub
		return dao.update(book);
	}

	@Override
	public int delete(String isbn) throws SQLException {
		// TODO Auto-generated method stub
		return dao.delete(isbn);
	}

}
