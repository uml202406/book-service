package com.ssafy.ws.step3.service;

import java.sql.SQLException;
import java.util.List;

import com.ssafy.ws.step3.dto.Book;

public interface BookService {
    /**
     * Book 정보를 DB에 저장한다.
     *
     * @param book 저장할 Book 정보
     * @return 저장된 책의 개수
     * @throws SQLException
     */
    int insert(Book book) throws SQLException;

    /**
     * 전체 Book의 정보를 List에 담아서 반환한다.
     *
     * @return Book의 리스트
     * @throws SQLException
     */
    List<Book> select() throws SQLException;
    /**
     * isbn Book정보 반환한다.
     * @param isbn
     * @return book
     * @throws SQLException
     */
    Book findById(String isbn) throws SQLException;
    
    /**
     * book정보 수정
     * @param book
     * @return 
     * @throws SQLException
     */
    int update(Book book) throws SQLException;
    
    /**
     * isbn book 삭제
     * @param isbn
     * @return
     * @throws SQLException
     */
    int delete(String isbn) throws SQLException;
}
