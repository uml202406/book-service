package com.ssafy.ws.step3.service;

import java.sql.SQLException;

import org.mindrot.jbcrypt.BCrypt;

import com.ssafy.ws.step3.dao.UserDao;
import com.ssafy.ws.step3.dao.UserDaoImpl;
import com.ssafy.ws.step3.dto.User;

public class UserServiceImpl implements UserService {
   	private static UserService instance = new UserServiceImpl();
   	private UserDao dao;
	 
	private UserServiceImpl() {
		dao = UserDaoImpl.getInstance();
	}

	public static UserService getInstance() {
		return instance;
	}
	@Override
	public User select(String id) throws SQLException {
		// TODO Auto-generated method stub
		return dao.select(id);
	}

	@Override
	public int signup(User user) throws SQLException {
		// password 암호화
		String encryptPassword = BCrypt.hashpw(user.getPass(), BCrypt.gensalt());
		user.setPass(encryptPassword);
		return dao.signup(user);
	}

	@Override
	public boolean signin(String id, String password) throws SQLException {
		//dao select(id)호출 -> 암호된 password와 평문 password 비교
		User user = dao.select(id);
		if(BCrypt.checkpw(password, user.getPass()) ) return true;
//		if( user.getPass().equals(password)) return true;
		return false;
	}

}
